#lang info
(define collection "ariadne")
(define deps '("base") )
(define build-deps '("scribble-lib" "racket-doc" "rackunit-lib"))
(define scribblings '(("scribblings/ariadne.scrbl" ())))
(define pkg-desc "Description Here")
(define version "0.0")
