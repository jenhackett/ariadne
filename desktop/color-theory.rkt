#lang racket

(provide no-scheme analogy-scheme dark-analogy-scheme subtle-scheme dark-subtle-scheme (struct-out colour-scheme))

(define (rgb-to-hsv red green blue)
  (let* ([r (/ red 255)]
         [g (/ green 255)]
         [b (/ blue 255)]
         [M (max r g b)]
         [m (min r g b)]
         [C (- M m)]
         [Hprime (cond
                   [(= 0 C) 0]
                   [(= M r) (let ([h (/ (- g b) C)])
                                (if (> 0 h) (+ 6 h) h))]
                   [(= M g) (+ 2 (/ (- b r) C))]
                   [(= M b) (+ 4 (/ (- r g) C))])]
         [H (* 60 Hprime)]
         [S (if (= 0 M) 0 (/ C M))])
    (list (exact-round H) S M)))

(define (hsv-to-rgb hue saturation value)
  (let* ([C (* value saturation)]
         [Hprime (/ hue 60)]
         [m (- value C)])
    (let-values ([(r g b) (cond
                            [(<= 0 Hprime 1) (values (+ m C) (+ m (* C Hprime)) m)]
                            [(<= 1 Hprime 2) (values (+ m (* C (- 2 Hprime))) (+ m C) m)]
                            [(<= 2 Hprime 3) (values m (+ m C) (+ m (* C (- Hprime 2))))]
                            [(<= 3 Hprime 4) (values m (+ m (* C (- 4 Hprime))) (+ m C))]
                            [(<= 4 Hprime 5) (values (+ m (* C (- Hprime 4))) m (+ m C))]
                            [(<= 5 Hprime 6) (values (+ m C) m (+ m (* C (- 6 Hprime))))])])
      (list (exact-round (* 255 r)) (exact-round (* 255 g)) (exact-round (* 255 b))))))

; for testing
(define (back-and-forth r g b)
  (apply hsv-to-rgb (rgb-to-hsv r g b)))
(define (forth-and-back h s v)
  (apply rgb-to-hsv (hsv-to-rgb h s v)))

(struct colour-scheme [background-colour scrollbar-colour gutter-colour base-font header link hover-link])

(define no-scheme (colour-scheme #f #f #f #f #f #f #f))

(define (analogy-scheme hue)
    (colour-scheme
     (hsv-to-rgb hue 0.1 1)
     (hsv-to-rgb hue 0.5 1)
     (hsv-to-rgb hue 0.3 0.7)
     (hsv-to-rgb hue 0.8 0.1)
     (hsv-to-rgb (modulo (- hue 30) 360) 0.5 0.6)
     (hsv-to-rgb (modulo (+ hue 30) 360) 0.6 0.7)
     (hsv-to-rgb (modulo (+ hue 30) 360) 0.8 1)))
(define (dark-analogy-scheme hue)
    (colour-scheme
     (hsv-to-rgb hue 0.1 0.3)
     (hsv-to-rgb hue 0.5 0.3)
     (hsv-to-rgb hue 0.3 0.6)
     (hsv-to-rgb hue 0.2 1)
     (hsv-to-rgb (modulo (- hue 30) 360) 0.5 1)
     (hsv-to-rgb (modulo (+ hue 30) 360) 0.6 0.8)
     (hsv-to-rgb (modulo (+ hue 30) 360) 0.9 1)))

(define (subtle-scheme hue)
    (colour-scheme
     (hsv-to-rgb hue 0 1)
     (hsv-to-rgb hue 0 0.3)
     (hsv-to-rgb hue 0 0.5)
     (hsv-to-rgb hue 0 0)
     (hsv-to-rgb hue 0 0)
     (hsv-to-rgb hue 0.6 0.7)
     (hsv-to-rgb hue 1 0.9)))
(define (dark-subtle-scheme hue)
    (colour-scheme
     (hsv-to-rgb hue 0.1 0.3)
     (hsv-to-rgb hue 0 0.1)
     (hsv-to-rgb hue 0 0.5)
     (hsv-to-rgb hue 0 1)
     (hsv-to-rgb hue 0.5 1)
     (hsv-to-rgb hue 0.6 0.8)
     (hsv-to-rgb hue 0.4 0.9)))