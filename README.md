ariadne
=======

Ariadne is a Racket #lang for writing Twine-style interactive fiction games that run on a desktop.

Notes on the code
-----------------
* Most of the interesting stuff is in desktop/forms and desktop/sdlrenderer

* web is an older version, as is desktop/renderer and desktop/style. (I really need to do some cleanup of obsolete stuff tbh)

* test.rkt is a file that shows some examples of the actual code

Running this
------------
To run this, you'll need to install SDL2, SDL2_TTF and SDL2_mixer, as well as the standard Racket tooling.
